use crate::tree::NodeId;

#[derive(Debug, Copy, Clone)]
/// An error that occurs when attempting to move a node to an invalid location
pub enum MoveError {
    /// The specified location referenced a node ID that doesn't exist
    NoSuchNode(NodeId),
    /// Attempted to move the node as a sibling of the root node; this is not allowed
    RootCannotHaveSiblings,
    /// Attempted to move a node under itself (either as a direct child or indirect descendant)
    ///
    /// This is not allowed, as it would result in an invalid Tree structure
    CannotMoveUnderSelf,
}

#[derive(Debug, Copy, Clone)]
/// An error that occurs when attempting to insert a node in an invalid location
pub enum InvalidLocation {
    /// The specified location referenced a node ID that doesn't exist
    NoSuchNode(NodeId),
    /// Attempted to place the node as a sibling of the root node; this is not allowed
    RootCannotHaveSiblings,
}

/// An error that occurs when attempting to remove the root node from a [Tree](super::Tree)
#[derive(Debug, Copy, Clone)]
pub struct CannotRemoveRoot;

#[derive(Debug, Copy, Clone)]
pub(crate) enum RemoveByIdError {
    NoSuchNode(NodeId),
    CannotRemoveRoot,
}

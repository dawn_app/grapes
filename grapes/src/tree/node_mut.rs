// false positives i think
#![allow(clippy::mut_mut)]

use crate::tree::error::{CannotRemoveRoot, RemoveByIdError};
use crate::tree::{Children, Location, MoveError, NodeId, Tree};

/// A mutable reference to a node in a Tree
///
/// The library guarantees that this references a valid node in the associated tree
#[derive(Debug)]
pub struct NodeMut<'a, T: Clone> {
    tree: &'a mut Tree<T>,
    index: NodeId,
}

impl<'a, T: Clone> NodeMut<'a, T> {
    pub(crate) fn new(tree: &'a mut Tree<T>, index: NodeId) -> Self {
        Self { tree, index }
    }
}

impl<'a, T: Clone> NodeMut<'a, T> {
    /// Get the data associated with the node
    #[must_use]
    pub fn data(&self) -> &T {
        // todo: new lookup every time, bad. should we cache the pointer somehow since it can't change?
        &self.tree.arena[self.index.0].data
    }

    /// Get the node's ID
    #[must_use]
    pub fn id(&self) -> NodeId {
        self.index
    }

    /// Iterate over the node's children
    pub fn children(&self) -> Children<T> {
        let first_child = self.tree.arena[self.index.0].first_child;
        Children::new(self.tree, first_child)
    }
}

/// Error: the root node cannot have siblings
#[derive(Copy, Clone, Debug)]
pub struct RootCannotHaveSiblings();

impl<'a, T: Clone> NodeMut<'a, T> {
    /// Get a mutable reference to the node's data
    #[must_use]
    pub fn data_mut(&mut self) -> &mut T {
        &mut self.tree.arena[self.index.0].data
    }

    /// Convert into a mutable reference to the node's data with the same lifetime as the tree
    #[must_use]
    pub fn into_data_mut(self) -> &'a mut T {
        &mut self.tree.arena[self.index.0].data
    }

    /// Push a new child node to the front of the children list
    ///
    /// This operation should always succeed
    ///
    /// (The specified value will become the first child, and any subsequent children will be pushed down)
    pub fn push_front_child(&mut self, value: T) -> NodeMut<T> {
        let tree = &mut self.tree;

        let new_id = tree.create_node(value);
        tree.attach(new_id, Location::FirstChildOf(self.index))
            .expect("Pushing child of existing node cannot fail");

        NodeMut::new(self.tree, new_id)
    }

    /// Push a new child as the next sibling of this node
    ///
    /// This is allowed for all nodes except for the root node. When attempting to add a sibling to the root node, this will return [None], and nothing will be added to the tree
    pub fn push_next_sibling(&mut self, value: T) -> Result<NodeMut<T>, RootCannotHaveSiblings> {
        if self.index == self.tree.root {
            return Err(RootCannotHaveSiblings());
        }

        let tree = &mut self.tree;

        let new_id = tree.create_node(value);

        match tree.attach(new_id, Location::AfterSibling(self.index)) {
            Ok(_) => Ok(NodeMut::new(self.tree, new_id)),
            Err(_) => unreachable!(),
        }
    }

    /// Remove this node and all of its descendants from the tree
    ///
    /// Note: if you'd like to do something with the removed values, use `remove_with_consumer` instead.
    ///
    /// Removing the root node is not allowed and will return an error with [CannotRemoveRoot]. All other removals are allowed.
    pub fn remove(self) -> Result<(), CannotRemoveRoot> {
        self.remove_with_consumer(|_| {})
    }

    /// Remove this node and all of its descendants from the tree
    ///
    /// All removed values will be passed to the specified `consumer` function
    ///
    /// Removing the root node is not allowed and will return an error with [CannotRemoveRoot]. All other removals are allowed.
    pub fn remove_with_consumer(
        mut self,
        mut consumer: impl FnMut(T),
    ) -> Result<(), CannotRemoveRoot> {
        let tree = &mut self.tree;
        match tree.detach(self.index) {
            Ok(_) => {}
            Err(RemoveByIdError::CannotRemoveRoot) => return Err(CannotRemoveRoot),
            Err(RemoveByIdError::NoSuchNode(_)) => {
                unreachable!()
            }
        };
        self.remove_node_hierarchy(self.index, &mut consumer);

        Ok(())
    }

    /// Removes the node and all children;
    /// Returns next sibling ID
    fn remove_node_hierarchy(
        &mut self,
        id: NodeId,
        consumer: &mut impl FnMut(T),
    ) -> Option<NodeId> {
        let tree = &mut self.tree;
        let removed = tree
            .arena
            .remove(id.0)
            .expect("Cannot remove nonexistent node");

        if let Some(first_child) = removed.first_child {
            let mut next_remove = first_child;

            while let Some(next_sibling) = self.remove_node_hierarchy(next_remove, consumer) {
                next_remove = next_sibling;
            }
        }

        (consumer)(removed.data);
        removed.next_sibling
    }

    /// Move this node, along with all descendants, to a new location
    ///
    /// If this is not allowed, a [MoveError] will be returned, and no changes will be made. See the [MoveError] documentation for details on possible errors.
    ///
    /// Moving a node to `AfterSibling(self ID)` is allowed and has no effect.
    pub fn move_to(&mut self, location: Location) -> Result<(), MoveError> {
        if let Location::AfterSibling(sibling) = location {
            if sibling == self.index {
                // placing after self is allowed but has no effect
                return Ok(());
            }
        }

        self.validate_new_location(&location)?;

        let tree = &mut self.tree;
        tree.detach(self.index).expect("Could not detach");
        tree.attach(self.index, location)
            .expect("Could not attach; location already validated");

        Ok(())
    }

    fn validate_new_location(&mut self, location: &Location) -> Result<(), MoveError> {
        match *location {
            Location::AfterSibling(node) => {
                if self.tree.root == node {
                    return Err(MoveError::RootCannotHaveSiblings);
                }

                match self.is_under_self(node) {
                    Some(true) => Err(MoveError::CannotMoveUnderSelf),
                    None => Err(MoveError::NoSuchNode(node)),
                    _ => Ok(()),
                }
            }
            Location::FirstChildOf(node) => match self.is_under_self(node) {
                Some(true) => Err(MoveError::CannotMoveUnderSelf),
                None => Err(MoveError::NoSuchNode(node)),
                _ => Ok(()),
            },
        }
    }

    /// Returns true if specified node is either self or a descendant
    ///
    /// Returns [None] if no such node.
    #[must_use]
    fn is_under_self(&self, id: NodeId) -> Option<bool> {
        if id == self.index {
            return Some(true);
        }

        let node = self.tree.arena.get(id.0)?;

        if let Some(parent) = node.parent {
            return self.is_under_self(parent);
        }

        Some(false)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn data_mut_twice() {
        let mut tree = Tree::new_with_root(0);
        let mut root = tree.root_mut();
        *root.data_mut() += 1;
        *root.data_mut() += 1;

        assert_eq!(*root.data(), 2)
    }

    #[test]
    fn return_data_mut() {
        // compile-time test to make sure we can return data_mut references from functions

        #[allow(dead_code)]
        fn root_data_mut(tree: &mut Tree<i32>) -> &mut i32 {
            let root = tree.root_mut();
            root.into_data_mut()
        }
    }
}

use crate::graph;
use crate::map_graph::{EdgeEntry, NodeEntry, NodeRef};
use std::hash::Hash;

/// Reference to an edge in a [MapGraph](super::MapGraph)
///
/// The library guarantees that this references a valid node
#[derive(Clone, Copy, Debug)]
pub struct EdgeRef<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::EdgeRef<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > EdgeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::EdgeRef<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }

    /// The data associated with this edge
    ///
    /// (aka the edge weight)
    #[must_use]
    pub fn data(&self) -> &'a EdgeData {
        &self.inner.data().data
    }

    /// The edge's identifier
    ///
    /// You can use this to later reference the edge in the graph (if it still exists)
    #[must_use]
    pub fn key(&self) -> &'a EdgeKey {
        &self.inner.data().key
    }

    /// Reference to the Node from which the edge starts
    ///
    /// (aka the tail of the arc)
    #[must_use]
    pub fn get_from_node(&self) -> NodeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData> {
        NodeRef::new(self.inner.get_from_node())
    }

    /// Reference to the Node the edge points to
    ///
    /// (aka the head of the arc)
    #[must_use]
    pub fn get_to_node(&self) -> NodeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData> {
        NodeRef::new(self.inner.get_to_node())
    }
}

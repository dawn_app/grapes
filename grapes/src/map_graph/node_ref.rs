use crate::graph;
use crate::map_graph::{EdgeEntry, EdgesFrom, EdgesTo, NodeEntry};
use std::hash::Hash;

/// Reference to a node in a [MapGraph](super::MapGraph)
///
/// The library guarantees that this references a valid node
#[derive(Clone, Copy, Debug)]
pub struct NodeRef<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::NodeRef<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > NodeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub(crate) fn new(
        inner: graph::NodeRef<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }

    /// The data associated with the node
    ///
    /// (a.k.a. the node weight)
    #[must_use]
    pub fn data(&self) -> &'a NodeData {
        &self.inner.data().data
    }

    /// The node's identifier
    ///
    /// You can use this to later reference the node in the graph (if it still exists)
    #[must_use]
    pub fn key(&self) -> &'a NodeKey {
        &self.inner.data().key
    }

    /// Iterate over edges leaving this node
    ///
    /// Order is unspecified.
    pub fn iter_edges_from(&self) -> EdgesFrom<NodeKey, NodeData, EdgeKey, EdgeData> {
        EdgesFrom::new(self.inner.iter_edges_from())
    }

    /// Iterate over edges entering this node
    ///
    /// Order is unspecified.
    pub fn iter_edges_to(&self) -> EdgesTo<NodeKey, NodeData, EdgeKey, EdgeData> {
        EdgesTo::new(self.inner.iter_edges_to())
    }
}

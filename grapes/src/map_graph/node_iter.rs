use crate::graph;
use crate::map_graph::{EdgeEntry, NodeEntry, NodeRef};
use std::hash::Hash;

/// Iterator over all nodes in a Graph
///
/// Order is unspecified
#[must_use]
pub struct GraphNodes<
    'a,
    NodeKey: Hash + Eq + Clone,
    NodeData: Clone,
    EdgeKey: Hash + Eq + Clone,
    EdgeData: Clone,
> {
    inner: graph::GraphNodes<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > GraphNodes<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    pub fn new(
        inner: graph::GraphNodes<'a, NodeEntry<NodeKey, NodeData>, EdgeEntry<EdgeKey, EdgeData>>,
    ) -> Self {
        Self { inner }
    }
}

impl<
        'a,
        NodeKey: Hash + Eq + Clone,
        NodeData: Clone,
        EdgeKey: Hash + Eq + Clone,
        EdgeData: Clone,
    > Iterator for GraphNodes<'a, NodeKey, NodeData, EdgeKey, EdgeData>
{
    type Item = NodeRef<'a, NodeKey, NodeData, EdgeKey, EdgeData>;

    fn next(&mut self) -> Option<Self::Item> {
        let inner = self.inner.next()?;

        Some(NodeRef::new(inner))
    }
}

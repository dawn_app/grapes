#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
// error return types are self-documenting imo
#![allow(clippy::missing_errors_doc)]
// panics are bugs
#![allow(clippy::missing_panics_doc)]
// let me name my stuff how i want
#![allow(clippy::module_name_repetitions)]
#![warn(missing_docs)]

//! # Grapes: Persistent Graph Data Structures
//!
//! The data structures are persistent: Cheap to clone, so you can keep (and modify) older versions of the data structure easily.
//!
//! Data Structures:
//! - [Tree](tree::Tree): collection of nodes with ordered children
//! - [MapTree](map_tree::MapTree): maintains a key-node mapping in addition to a tree
//! - [Graph](graph::Graph): collection of nodes connected by edges
//! - [MapGraph](map_graph::MapGraph): also maintains a key-node and key-edge mapping
//! - [Arena](arena::Arena): for building arbitrary graph-like data structures

mod fuzz_test;

pub mod arena;

pub mod tree;

pub mod map_tree;

pub mod graph;

pub mod map_graph;

/// An error that occurs when attempting to move a node to an invalid location
#[derive(Debug, Copy, Clone)]
pub enum MoveError<Key> {
    /// The specified location referenced a node key that doesn't exist
    NoSuchNode(Key),
    /// Attempted to move the node as a sibling of the root node; this is not allowed
    RootCannotHaveSiblings,
    /// Attempted to move a node under itself (either as a direct child or indirect descendant)
    ///
    /// This is not allowed, as it would result in an invalid Tree structure
    CannotMoveUnderSelf,
}

/// An error that occurs when attempting to insert a node in an invalid location
#[derive(Debug, Copy, Clone)]
pub enum InvalidLocation<Key> {
    /// The specified location referenced a node key that doesn't exist
    NoSuchNode(Key),
    /// Attempted to place the node as a sibling of the root node; this is not allowed
    RootCannotHaveSiblings,
}

/// An error that occurs when attempting to remove the root node from a [MapTree](super::MapTree)
#[derive(Debug, Copy, Clone)]
pub struct CannotRemoveRoot;

use crate::map_tree::{Entry, NodeRef};
use crate::tree;
use std::hash::Hash;

/// An iterator over the children of a node
#[derive(Clone, Debug)]
#[must_use]
pub struct Children<'a, Key: Hash + Eq + Clone, T: Clone> {
    inner: tree::Children<'a, Entry<Key, T>>,
}

impl<'a, Key: Hash + Eq + Clone, T: Clone> Children<'a, Key, T> {
    pub(crate) fn new(inner: tree::Children<'a, Entry<Key, T>>) -> Self {
        Self { inner }
    }
}

impl<'a, Key: Hash + Eq + Clone, T: Clone> Iterator for Children<'a, Key, T> {
    type Item = NodeRef<'a, Key, T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|inner_node| NodeRef::new(inner_node))
    }
}

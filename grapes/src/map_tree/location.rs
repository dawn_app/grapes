use crate::tree;
use crate::tree::NodeId;

/// Describes a location in a tree
#[derive(Copy, Clone, Debug)]
pub enum Location<Key> {
    /// The next sibling of the specified node
    ///
    /// Inserting a node here would make that node the next sibling after the specified node, and push any further children down
    ///
    /// Note: the specified node should not be the root node, as the root may have no siblings
    AfterSibling(Key),
    /// The first child of the specified node
    ///
    /// Inserting a node here would make that node the first child, and push any other children down
    FirstChildOf(Key),
}

impl<Key> Location<Key> {
    /// Map the inner ID type of the Location
    pub(crate) fn into_tree_location<E>(
        self,
        mapper: impl FnOnce(Key) -> Result<NodeId, E>,
    ) -> Result<tree::Location, E> {
        Ok(match self {
            Location::AfterSibling(id) => tree::Location::AfterSibling((mapper)(id)?),
            Location::FirstChildOf(id) => tree::Location::FirstChildOf((mapper)(id)?),
        })
    }
}

# Grapes: Persistent Graph Data Structures

[Persistent](https://en.wikipedia.org/wiki/Persistent_data_structure): Cheap to clone, so you can keep (and modify) older versions of the data structure easily.

Data Structures:
- [Tree](https://docs.rs/grapes/latest/grapes/tree/): hierarchical, indexable collection with ordered children
- [MapTree](https://docs.rs/grapes/latest/grapes/map_tree/): maintains a key-node mapping in addition to a tree
- [Graph](https://docs.rs/grapes/latest/grapes/graph/): collection of nodes connected by edges
- [MapGraph](https://docs.rs/grapes/latest/grapes/map_graph/): also maintains a key-node and key-edge mapping
- [Arena](https://docs.rs/grapes/latest/grapes/arena/): for building arbitrary graph-like data structures

## Alternatives

Unless you specifically need a *persistent* tree or a *persistent* graph, I recommend using these crates instead (these are more mature, and probably faster):

- Arenas: [generational-arena](https://crates.io/crates/generational-arena)
- Trees: [id_tree](https://crates.io/crates/id_tree)
- Graphs: [petgraph](https://crates.io/crates/petgraph)

If you need simple persistent data structures (lists, maps), check out:

- [im](https://crates.io/crates/im) or [imbl](https://crates.io/crates/imbl)
- [rpds](https://crates.io/crates/rpds)

## Status

This library only supports what I need it to support for my very specific use case.

There are no benchmarks; performance is probably nowhere near other libraries, and this is fine for me.

## Use Cases

This library is only useful if you need graph structures that can be somewhat cheaply cloned and manipulated.

For example, if you're building an application that stores state in a graph structure, and needs efficient undo-redo
functionality, you might want to store past copies of the graph in memory. To do this efficiently, those copies should
share parts of the graph that didn't change.
use id_tree::{NodeId, Tree};
use rand::{thread_rng, Rng};

use crate::{CreateNew, Location, Operation, TreeManipulation};

struct ValueGenerator {
    next_value: i64,
}

impl ValueGenerator {
    pub fn new() -> Self {
        Self { next_value: 0 }
    }

    pub fn next_value(&mut self) -> i64 {
        let value = self.next_value;
        self.next_value += 1;

        value
    }
}

pub struct StepGenerator {
    values: ValueGenerator,
    tree: id_tree::Tree<i64>,
}

impl StepGenerator {
    pub fn new() -> (CreateNew<NodeId, i64>, Self) {
        let mut values = ValueGenerator::new();

        let value = values.next_value();
        let (root_id, tree) = Tree::new_with_root(value);

        let operation = CreateNew {
            reference_id: root_id,
            value,
        };
        let generator = Self { tree, values };

        (operation, generator)
    }

    pub fn generate_random_operation(&mut self) -> Operation<NodeId, i64> {
        let node = self.get_random_node();

        let root = self.tree.root_node_id().expect("no root");

        if &node == root {
            self.generate_insert(node)
        } else {
            // todo: move nodes
            match thread_rng().gen_range(0..2) {
                0 => self.generate_insert(node),
                1 => self.generate_remove(node),
                _ => unreachable!(),
            }
        }
    }

    fn generate_insert(&mut self, id: NodeId) -> Operation<NodeId, i64> {
        let root = self.tree.root_node_id().expect("no root");

        let location = if &id != root && thread_rng().gen_bool(0.7) {
            Location::NextSiblingOf(id)
        } else {
            Location::FirstChildOf(id)
        };

        let value = self.values.next_value();
        let reference_id = self.tree.insert_node(value, location.clone());
        

        Operation::Insert {
            value,
            location,
            reference_id,
        }
    }

    fn generate_remove(&mut self, id: NodeId) -> Operation<NodeId, i64> {
        self.tree.remove(&id);
        Operation::Remove(id)
    }

    /// Get a random node ID from the reference tree.
    fn get_random_node(&self) -> NodeId {
        let root = self.tree.root_node_id().expect("no root");
        let nodes: Vec<_> = self
            .tree
            .traverse_pre_order_ids(root)
            .expect("invalid node")
            .collect();

        let index = rand::thread_rng().gen_range(0..nodes.len());

        nodes[index].clone()
    }
    pub fn current_expected_tree(&self) -> &id_tree::Tree<i64> {
        &self.tree
    }
}

use std::fmt::Debug;

use crate::TreeTraversal;

#[derive(Debug)]
pub enum TreeInconsistency<T> {
    Value {
        a: T,
        b: T,
    },
    Child {
        index: usize,
        inconsistency: Box<TreeInconsistency<T>>,
    },
}

pub fn tree_equivalence<T: PartialEq + Clone>(
    a: &impl TreeTraversal<T>,
    b: &impl TreeTraversal<T>,
) -> Result<(), TreeInconsistency<T>> {
    let node_a = a.root();
    let node_b = b.root();

    node_equivalence(a, b, node_a, node_b)
}

fn node_equivalence<T: PartialEq + Clone, A: TreeTraversal<T>, B: TreeTraversal<T>>(
    a: &A,
    b: &B,
    a_node: A::NodeId,
    b_node: B::NodeId,
) -> Result<(), TreeInconsistency<T>> {
    let (data_a, children_a) = a.node(a_node);
    let (data_b, children_b) = b.node(b_node);

    if data_a != data_b {
        return Err(TreeInconsistency::Value {
            a: data_a.clone(),
            b: data_b.clone(),
        });
    }

    for (index, (child_a, child_b)) in children_a.zip(children_b).enumerate() {
        node_equivalence(a, b, child_a.clone(), child_b.clone()).map_err(|inconsistency| {
            TreeInconsistency::Child {
                index,
                inconsistency: Box::new(inconsistency),
            }
        })?;
    }

    Ok(())
}

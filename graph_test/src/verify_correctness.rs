use id_tree::NodeId;

use crate::generator::StepGenerator;
use crate::{tree_equivalence, CreateNew, IdMapping, Operation, TreeManipulation, TreeTraversal};

pub fn verify_correctness<T: TreeManipulation<i64> + TreeTraversal<i64>>() {
    let (operation, mut generator) = StepGenerator::new();
    let mut subject = TreeManipulator::<T>::new_with_operation(operation);

    let mut operations = Vec::new();
    for _ in 0..1000 {
        let operation = generator.generate_random_operation();

        operations.push(operation.clone());
        subject.apply(operation.clone());
        if let Operation::Remove(_) = operation {
            let current_tree = generator.current_expected_tree();

            subject
                .map
                .retain(|node_id, _| current_tree.get(node_id).is_ok())
        }

        match tree_equivalence(generator.current_expected_tree(), subject.tree()) {
            Ok(_) => {}
            Err(error) => {
                panic!("Trees not equivalent: {error:#?}");
            }
        }
    }
}

struct TreeManipulator<T: TreeManipulation<i64>> {
    tree: T,
    map: IdMapping<T::Key>,
}

impl<T: TreeManipulation<i64>> TreeManipulator<T> {
    pub fn tree(&self) -> &T {
        &self.tree
    }
}

impl<T: TreeManipulation<i64>> TreeManipulator<T> {
    pub fn new_with_operation(operation: CreateNew<NodeId, i64>) -> Self {
        let (key, tree) = T::new_with_root(operation.value);
        let mut map = IdMapping::new();
        map.register(&operation.reference_id, key)
            .expect("mapping mismatch");

        Self { tree, map }
    }

    pub fn apply(&mut self, operation: Operation<NodeId, i64>) {
        match operation {
            Operation::Insert {
                value,
                location,
                reference_id,
            } => {
                let mapped_location = self.map.map_location(location);
                let id = self.tree.insert_node(value, mapped_location);
                self.map.register(&reference_id, id).expect("Id mismatch");
            }
            Operation::Move(from, to) => {
                let from_mapped = self.map.lookup(&from).expect("no such id");
                let to_mapped = self.map.map_location(to);

                self.tree.move_node(from_mapped, to_mapped);
            }
            Operation::Remove(id) => {
                let mapped = self.map.lookup(&id).expect("no such id");
                self.tree.remove(mapped);
                // note: map will be updated by caller
            }
        }
    }
}

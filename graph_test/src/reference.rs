use id_tree::{InsertBehavior, MoveBehavior, Node, NodeId, RemoveBehavior, Tree};

use crate::{Location, TreeManipulation, TreeTraversal};

fn get_parent_and_index<T>(tree: &Tree<T>, location: Location<NodeId>) -> (NodeId, usize) {
    match location {
        Location::FirstChildOf(id) => (id, 0),
        Location::NextSiblingOf(sibling) => {
            let parent = tree
                .ancestor_ids(&sibling)
                .expect("invalid node")
                .next()
                .expect("can't insert sibling of root");
            let index = tree
                .children_ids(parent)
                .expect("can't get children")
                .enumerate()
                .find_map(|(index, id)| if id == &sibling { Some(index) } else { None })
                .expect("cant find child under parent");

            (parent.clone(), index + 1)
        }
    }
}

impl<T> TreeManipulation<T> for Tree<T> {
    type Key = NodeId;

    fn new_with_root(value: T) -> (Self::Key, Self) {
        let mut tree = Tree::new();
        let id = tree
            .insert(Node::new(value), InsertBehavior::AsRoot)
            .expect("could not insert root");
        (id, tree)
    }

    fn insert_node(&mut self, value: T, location: Location<Self::Key>) -> Self::Key {
        let (parent, index) = get_parent_and_index(self, location);

        let new_index = self
            .insert(Node::new(value), InsertBehavior::UnderNode(&parent))
            .expect("could not insert");
        self.make_nth_sibling(&new_index, index)
            .expect("could not make_nth_sibling");

        new_index
    }

    fn move_node(&mut self, id: &Self::Key, new_location: Location<Self::Key>) {
        let (parent, index) = get_parent_and_index(self, new_location);

        self.move_node(id, MoveBehavior::ToParent(&parent))
            .expect("could not move");
        self.make_nth_sibling(id, index)
            .expect("could not make_nth_sibling");
    }

    fn remove(&mut self, id: &Self::Key) {
        self.remove_node(id.clone(), RemoveBehavior::DropChildren)
            .expect("could not remove");
    }
}

impl<T> TreeTraversal<T> for Tree<T> {
    type NodeId = NodeId;

    fn root(&self) -> Self::NodeId {
        self.root_node_id().expect("no root node").clone()
    }

    fn node(&self, node: Self::NodeId) -> (&T, Box<dyn Iterator<Item = Self::NodeId> + '_>) {
        let data = self.get(&node).expect("no such node").data();
        let children = self.children_ids(&node).expect("no such node").cloned();

        (data, Box::new(children))
    }
}
